
int synapse_i2c_init (void);
void synapse_i2c_uninit (void);
int synapse_i2c_process (void);

int synapse_i2c_read (uint8_t address, uint8_t *buffer, int size, int stop);
int synapse_i2c_write (uint8_t address, const uint8_t* buffer, int size, int stop);
