#include <asf.h>

#include <string.h>
#include "common.h"
#include "common_pinmux.h"
#include "common_console.h"

#define SYNAPSE_CONSOLE_INPUT_BUFFER_SIZE      32

static uint16_t g_console_input_char;
static uint16_t g_console_ignore_char_count;
static uint16_t g_console_input_buffer_written;
static uint8_t g_console_input_buffer[SYNAPSE_CONSOLE_INPUT_BUFFER_SIZE];

ssize_t _read (int fd, void *buf, size_t count);
ssize_t _write (int fd, const void *buf, size_t count);

static struct usart_module g_console_uart_module;

static void console_uart_callback (const struct usart_module *const module)
{
	(void) module;

	if (g_console_ignore_char_count > 0) {
		g_console_ignore_char_count--;
		return;
		} else if (g_console_input_char == 0x1B) { /* Ignore escape and following 2 characters. */
		g_console_ignore_char_count = 2;
		return;
	}

	/* If input string is bigger than buffer size limit, ignore the excess part. */
	if (g_console_input_buffer_written < SYNAPSE_CONSOLE_INPUT_BUFFER_SIZE) {
		g_console_input_buffer[g_console_input_buffer_written++] = g_console_input_char & 0xFF;
	}
}

int synapse_console_init (void)
{
	struct usart_config config_usart;

	g_console_input_char = 0;
	g_console_ignore_char_count = 0;
	g_console_input_buffer_written = 0;
	memset(g_console_input_buffer, 0, sizeof(g_console_input_buffer));

	memset(&g_console_uart_module, 0, sizeof(struct usart_module));

	usart_get_config_defaults(&config_usart);

	config_usart.baudrate    = 9600; //minimum 300'de calistik.
	config_usart.mux_setting = USART_RX_3_TX_2_XCK_3;
	config_usart.pinmux_pad0 = PINMUX_UNUSED;
	config_usart.pinmux_pad1 = PINMUX_UNUSED;
	config_usart.pinmux_pad2 = PINMUX_PA10D_SERCOM2_PAD2; // DevKit TX'i !
	config_usart.pinmux_pad3 = PINMUX_PA11D_SERCOM2_PAD3; // DevKit RX'i !
	while (usart_init(&g_console_uart_module, SERCOM2, &config_usart) != STATUS_OK) {
	}

	usart_register_callback(&g_console_uart_module, (usart_callback_t) console_uart_callback, USART_CALLBACK_BUFFER_RECEIVED);
	usart_enable_callback(&g_console_uart_module, USART_CALLBACK_BUFFER_RECEIVED);
	usart_enable(&g_console_uart_module);

	return 0;
}

int synapse_console_process (void)
{
	usart_read_job(&g_console_uart_module, &g_console_input_char);
	return 0;
}

void synapse_console_uninit (void)
{

}

ssize_t synapse_console_write_wait (const void *data, size_t count)
{
	enum status_code sc;
	sc = usart_write_buffer_wait(&g_console_uart_module, data, count);
	if (sc == STATUS_OK) {
		return count;
	}
	return -1;
}

ssize_t _read (int fd, void *buf, size_t count)
{
	if (buf == NULL) {
		return -1;
	}
	if (count <= 0) {
		return -1;
	}
	if (fd == STDIN_FILENO) {
		system_interrupt_enter_critical_section();
		count = MIN(count, g_console_input_buffer_written);
		memcpy(buf, g_console_input_buffer, count);
		memmove(g_console_input_buffer, g_console_input_buffer + count, g_console_input_buffer_written - count);
		g_console_input_buffer_written -= count;
		system_interrupt_leave_critical_section();
		return count;
	}
	return -1;
}

ssize_t _write (int fd, const void *buf, size_t count)
{
	if (fd == STDOUT_FILENO ||
	fd == STDERR_FILENO) {
		return synapse_console_write_wait(buf, count);
	}
	return -1;
}
