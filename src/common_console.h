#ifndef SERVICES_CONSOLE_H_
#define SERVICES_CONSOLE_H_

int synapse_console_init (void);
int synapse_console_process (void);
void synapse_console_uninit (void);

ssize_t synapse_console_write_wait (const void *data, size_t count);

#endif /* SERVICES_CONSOLE_H_ */
