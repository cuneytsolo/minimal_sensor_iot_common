
#include <asf.h>

#include "common_time.h"

int _gettimeofday (struct timeval *tp, void *tzp);

static struct rtc_module g_rtc_module;

static volatile uint32_t g_overflows    = 0;
static const uint32_t    g_resolution   = 10;

static void rtc_overflow_callback (void)
{
        g_overflows += 1;
}

int synapse_time_init (void)
{
        struct rtc_count_config config_rtc_count;

        rtc_count_get_config_defaults(&config_rtc_count);
        config_rtc_count.prescaler           = RTC_COUNT_PRESCALER_DIV_1;
        config_rtc_count.mode                = RTC_COUNT_MODE_16BIT;
        config_rtc_count.continuously_update = true;
        config_rtc_count.compare_values[0]   = g_resolution;
        rtc_count_init(&g_rtc_module, RTC, &config_rtc_count);
        rtc_count_enable(&g_rtc_module);

        rtc_count_register_callback(&g_rtc_module, rtc_overflow_callback, RTC_COUNT_CALLBACK_OVERFLOW);
        rtc_count_enable_callback(&g_rtc_module, RTC_COUNT_CALLBACK_OVERFLOW);

        rtc_count_set_period(&g_rtc_module, g_resolution);

        return 0;
}

void synapse_time_uninit (void)
{

}

int synapse_time_process (void)
{
        return 0;
}

int _gettimeofday (struct timeval *tp, void *tzp)
{
        uint32_t overflows;
        (void) tzp;
        system_interrupt_enter_critical_section();
        overflows = g_overflows;
        system_interrupt_leave_critical_section();
        tp->tv_sec = overflows / (1000 / g_resolution);
        tp->tv_usec = (overflows % (1000 / g_resolution)) * g_resolution * 1000;
        return 0;
}
