
#include <asf.h>

#include "common_clock.h"

static const int g_clock_use_external_oscillator = 0;

int synapse_clock_init (void)
{
        if (g_clock_use_external_oscillator) {
                // External 8MHz oscillator config
                struct system_clock_source_xosc_config config_xosc;
                system_clock_source_xosc_get_config_defaults(&config_xosc);
                config_xosc.frequency = 8000000UL;
                system_clock_source_xosc_set_config(&config_xosc);
                system_clock_source_enable(SYSTEM_CLOCK_SOURCE_XOSC);

                // Flash wait state before clock source change
                system_flash_set_waitstates(2);

                // Generator 0 - External 8M oscillator
                struct system_gclk_gen_config config_gclk;
                system_gclk_gen_get_config_defaults(&config_gclk);
                config_gclk.source_clock = SYSTEM_CLOCK_SOURCE_XOSC;
                config_gclk.division_factor = 1;
                system_gclk_gen_set_config(GCLK_GENERATOR_0, &config_gclk);
        }

        // Generator 2 - Internal 32K oscillator divided by 32, 1KHz
        struct system_gclk_gen_config config_gclk;
        system_gclk_gen_get_config_defaults(&config_gclk);
        config_gclk.source_clock = GCLK_SOURCE_OSC32K;
        config_gclk.division_factor = 32;
        system_gclk_gen_set_config(GCLK_GENERATOR_2,&config_gclk);

        return 0;
}

void synapse_clock_uninit (void)
{

}

int synapse_clock_process (void)
{
        return 0;
}
