
int synapse_pwm_init (void);
void synapse_pwm_uninit (void);
int synapse_pwm_process (void);

int synapse_pwm_start (void);
int synapse_pwm_stop (void);
int synapse_pwm_run (uint32_t duration);
