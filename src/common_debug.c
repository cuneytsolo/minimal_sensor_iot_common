#include <stdio.h>
#include <stdarg.h>
#include <sys/time.h>

#include "common_debug.h"

static unsigned int g_debug_level = SYNAPSE_DEBUG_LEVEL_INFO;

static const char * debug_level_string(unsigned int level);

static const char * debug_level_string(unsigned int level)
{
	if (level <= SYNAPSE_DEBUG_LEVEL_ERROR){
		return "error";
		} else if (level <= SYNAPSE_DEBUG_LEVEL_INFO){
		return "info";
		} else if (level <= SYNAPSE_DEBUG_LEVEL_DEBUG){
		return "debug";
		} else {
		return "noisy";
	}
}

int synapse_debug_init(void)
{
	return 0;
}

int synapse_debug_process(void)
{
	return 0;
}

void synapse_debug_uninit(void)
{

}

unsigned int synapse_debug_get_level(void)
{
	return g_debug_level;
};

void synapse_debug_set_level(unsigned int level)
{
	g_debug_level = level;
};

void synapse_debug_printf (unsigned int level, const char *function, const char *file, unsigned int line, const char *format, ...)
{
	va_list ap;
	struct timeval timeval;
	if (g_debug_level < level) {
		return;
	}
	gettimeofday(&timeval, NULL);
	//timeval.tv_sec = 1;
	//timeval.tv_usec = 1000;

	fprintf(stderr, "\033[0G[%ld.%03ld %s] ", timeval.tv_sec, timeval.tv_usec / 1000, debug_level_string(level));
	va_start(ap, format);
	vfprintf(stderr, format, ap);
	va_end(ap);
	fprintf(stderr, " @ %s %s:%d\r\n\033[K", function, file, line);
}
