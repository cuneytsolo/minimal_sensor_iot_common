
#include <asf.h>

#include "common_pinmux.h"
#include "common_i2c.h"
#include "common_debug.h"

static struct i2c_master_module g_i2c_master_instance;

int synapse_i2c_init (void)
{
        struct i2c_master_config config_i2c_master;
        i2c_master_get_config_defaults(&config_i2c_master);
        config_i2c_master.start_hold_time       = I2C_MASTER_START_HOLD_TIME_400NS_800NS;
        config_i2c_master.generator_source      = GCLK_GENERATOR_0;
        config_i2c_master.baud_rate             = 20; //1KHz
        config_i2c_master.pinmux_pad0           = PINMUX_I2C_SDA;
        config_i2c_master.pinmux_pad1           = PINMUX_I2C_SCL;
        /* Initialize and enable device with config. */
        i2c_master_init(&g_i2c_master_instance, SERCOM5, &config_i2c_master);
        i2c_master_enable(&g_i2c_master_instance);
        return 0;
}

void synapse_i2c_uninit (void)
{

}

int synapse_i2c_process (void)
{
        return 0;
}

int synapse_i2c_reset (void)
{
        i2c_master_reset(&g_i2c_master_instance);
        synapse_i2c_init();
        return 0;
}

int synapse_i2c_read (uint8_t address, uint8_t *buffer, int size, int stop)
{
        enum status_code opresult;
        struct i2c_master_packet packet = {
                .address                = address,
                .data_length            = size,
                .data                   = buffer,
                .ten_bit_address        = false,
                .high_speed             = false,
                .hs_master_code         = 0x0,
        };
        if (stop) {
                opresult = i2c_master_read_packet_wait(&g_i2c_master_instance, &packet);
        } else {
                opresult = i2c_master_read_packet_wait_no_stop(&g_i2c_master_instance, &packet);
        }
        if (opresult == STATUS_OK) {
                return 1;
        } else {
                return 0;
        }
}

int synapse_i2c_write (uint8_t address, const uint8_t* buffer, int size, int stop)
{
        enum status_code opresult;
        struct i2c_master_packet packet = {
                .address                = address,
                .data_length            = size,
                .data                   = (uint8_t *)buffer,
                .ten_bit_address        = false,
                .high_speed             = false,
                .hs_master_code         = 0x0,
        };
        if (stop) {
                opresult= i2c_master_write_packet_wait(&g_i2c_master_instance, &packet);
        } else {
                opresult= i2c_master_write_packet_wait_no_stop(&g_i2c_master_instance, &packet);
        }
        if (opresult == STATUS_OK) {
                return 1;
        } else {
                return 0;
        }
}

