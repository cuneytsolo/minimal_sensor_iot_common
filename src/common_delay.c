
#undef SYNAPSE_DEBUG_MODULE
#define SYNAPSE_DEBUG_MODULE SYNAPSE_DEBUG_MODULE_DELAY

#include <asf.h>

#include "delay.h"

int synapse_delay_init (void)
{
        delay_init();
        return 0;
}

void synapse_delay_uninit (void)
{

}

int synapse_delay_process (void)
{
        return 0;
}

void synapse_delay_sleep (uint32_t s)
{
        delay_s(s);
}

void synapse_delay_msleep (uint32_t ms)
{
        delay_ms(ms);
}

void synapse_delay_usleep (uint32_t us)
{
        delay_us(us);
}
