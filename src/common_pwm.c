
#include <asf.h>

#include "common_delay.h"
#include "common_pinmux.h"
#include "common_pwm.h"

static struct tc_module tc_instance;

int synapse_pwm_init (void)
{
        struct tc_config config_tc;
        tc_get_config_defaults(&config_tc);

        config_tc.clock_prescaler = TC_CLOCK_PRESCALER_DIV8;
        config_tc.counter_size    = TC_COUNTER_SIZE_8BIT;
        config_tc.wave_generation = TC_WAVE_GENERATION_NORMAL_PWM;
        config_tc.counter_8_bit.compare_capture_channel[0] = (0xFF / 2);
        config_tc.counter_8_bit.period= 0xFF;

        config_tc.pwm_channel[0].enabled = true;
        config_tc.pwm_channel[0].pin_out = PIN_PWM0;
        config_tc.pwm_channel[0].pin_mux = PINMUX_PWM0;

        tc_init(&tc_instance, TC7, &config_tc);

        return 0;
}

void synapse_pwm_uninit (void)
{

}

int synapse_pwm_process (void)
{
        return 0;
}

int synapse_pwm_start (void)
{
        tc_enable(&tc_instance);
        return 0;
}

int synapse_pwm_stop (void)
{
        tc_disable(&tc_instance);
        return 0;
}

int synapse_pwm_run (uint32_t duration)
{
        synapse_pwm_start();
        synapse_delay_msleep(50);
        synapse_pwm_stop();
        return 0;
}
