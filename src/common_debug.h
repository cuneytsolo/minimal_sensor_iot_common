#ifndef DEBUG_H_
#define DEBUG_H_

int synapse_debug_init(void);
int synapse_debug_process(void);
void synapse_debug_uninit(void);

unsigned int synapse_debug_get_level(void);
void synapse_debug_set_level(unsigned int level);

enum{
	SYNAPSE_DEBUG_LEVEL_SILENT	= 0,
	SYNAPSE_DEBUG_LEVEL_ERROR	= 1,
	SYNAPSE_DEBUG_LEVEL_INFO	= 2,
	SYNAPSE_DEBUG_LEVEL_DEBUG	= 3,
	};

#define synapse_errorf(a...)            synapse_debug_printf(SYNAPSE_DEBUG_LEVEL_ERROR, __FUNCTION__, __FILE__, __LINE__, a)
#define synapse_infof(a...)             synapse_debug_printf(SYNAPSE_DEBUG_LEVEL_INFO, __FUNCTION__, __FILE__, __LINE__, a)
#define synapse_debugf(a...)            synapse_debug_printf(SYNAPSE_DEBUG_LEVEL_DEBUG, __FUNCTION__, __FILE__, __LINE__, a)

void synapse_debug_printf (unsigned int level, const char *function, const char *file, unsigned int line, const char *format, ...) __attribute__((format(printf, 5, 6)));

#endif /* DEBUG_H_ */
