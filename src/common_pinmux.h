#ifndef PINMUX_H_
#define PINMUX_H_

#define PIN_LED			PIN_PB10
#define PIN_BUTTON              PIN_PA03
//#define PIN_SERCOM4_UNUSED	    PIN_PB10

#define PINMUX_I2C_SDA          PINMUX_PA22D_SERCOM5_PAD0
#define PINMUX_I2C_SCL          PINMUX_PA23D_SERCOM5_PAD1

//#define PINMUX_PWM0             PINMUX_PA06F_TC1_WO0
//#define PIN_ADC0                PIN_PA02
//#define PINMUX_ADC0             MUX_PA02B_ADC_AIN0

#define PIN_HC595_CLK           PIN_PA04
#define PIN_HC595_DATA          PIN_PA05
#define PIN_HC595_LATCH         PIN_PA07

#define PIN_PWM0                PIN_PA21
#define PIN_BUZZER              PIN_PA21
#define PINMUX_PWM0             PINMUX_PA21E_TC7_WO1

int synapse_pinmux_init(void);
int synapse_pinmux_process(void);
void synapse_pinmux_uninit(void);

void synapse_pinmux_setup (uint32_t pin, int dir, int level, int pullup);

void synapse_pinmux_set_level(const uint8_t gpio_pin, const bool level);
int synapse_pinmux_get_level(const uint8_t gpio_pin);
void synapse_pinmux_toggle_level(const uint8_t gpio_pin);

#endif /* PINMUX_H_ */
