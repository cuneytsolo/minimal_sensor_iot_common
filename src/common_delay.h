
int synapse_delay_init (void);
void synapse_delay_uninit (void);
int synapse_delay_process (void);
void synapse_delay_sleep (uint32_t s);
void synapse_delay_msleep (uint32_t ms);
void synapse_delay_usleep (uint32_t us);
