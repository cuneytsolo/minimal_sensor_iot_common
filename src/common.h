
#ifndef COMMON_H_
#define COMMON_H_

#if !defined(STDIN_FILENO)
#define STDIN_FILENO            0
#endif
#if !defined(STDOUT_FILENO)
#define STDOUT_FILENO           1
#endif
#if !defined(STDERR_FILENO)
#define STDERR_FILENO           2
#endif

#if !defined(MIN)
#define MIN(a, b)               (((a) < (b)) ? (a) : (b))
#endif

#if !defined(MAX)
#define MAX(a, b)               (((a) > (b)) ? (a) : (b))
#endif

#endif /* COMMON_H_ */
