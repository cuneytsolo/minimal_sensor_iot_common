#define synapse_timeval_isset(mts)     ((mts)->tv_sec || (mts)->tv_usec)
#define synapse_timeval_clear(mts)     ((mts)->tv_sec = (mts)->tv_usec = 0)

#define synapse_timeval_compare(a, b, CMP)                                \
        (((a)->tv_sec == (b)->tv_sec) ?                                 \
                ((a)->tv_usec CMP (b)->tv_usec) :                       \
                ((a)->tv_sec CMP (b)->tv_sec))

#define synapse_timeval_add(a, b, result)                                 \
        do {                                                            \
                (result)->tv_sec = (a)->tv_sec + (b)->tv_sec;           \
                (result)->tv_usec = (a)->tv_usec + (b)->tv_usec;        \
                if ((result)->tv_usec >= 1000000) {                     \
                        ++(result)->tv_sec;                             \
                        (result)->tv_usec -= 1000000;                   \
                }                                                       \
        } while (0)

#define synapse_timeval_sub(a, b, result)                                 ß\
        do {                                                            \
                (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;           \
                (result)->tv_usec = (a)->tv_usec - (b)->tv_usec;        \
                if ((result)->tv_usec < 0) {                            \
                        --(result)->tv_sec;                             \
                        (result)->tv_usec += 1000000;                   \
                }                                                       \
        } while (0)

int synapse_time_init(void);
int synapse_time_process(void);
void synapse_time_uninit(void);
