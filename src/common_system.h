#ifndef COMMON_SYSTEM_H_
#define COMMON_SYSTEM_H_

int synapse_system_init(void);
int synapse_system_process(void);
void synapse_system_uninit(void);

#endif /* COMMON_SYSTEM_H_ */

