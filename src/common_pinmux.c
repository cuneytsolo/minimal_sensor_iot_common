#include <asf.h>

#include "common_pinmux.h"



int synapse_pinmux_init(void)
{
	return 0;
}

int synapse_pinmux_process(void)
{
	return 0;
}

void synapse_pinmux_uninit(void)
{

}

void synapse_pinmux_setup (uint32_t pin, int dir, int level, int pullup)
{
        struct system_pinmux_config config_pinmux;
        system_pinmux_get_config_defaults(&config_pinmux);
        config_pinmux.mux_position      = SYSTEM_PINMUX_GPIO;
        config_pinmux.direction         = (dir) ? SYSTEM_PINMUX_PIN_DIR_OUTPUT : SYSTEM_PINMUX_PIN_DIR_INPUT;
        config_pinmux.input_pull        = (pullup) ? SYSTEM_PINMUX_PIN_PULL_UP : SYSTEM_PINMUX_PIN_PULL_NONE;
        system_pinmux_pin_set_config(pin, &config_pinmux);
        if (!dir) {
                system_pinmux_pin_set_input_sample_mode(pin, SYSTEM_PINMUX_PIN_SAMPLE_ONDEMAND);
        }
        port_pin_set_output_level(pin, level);
}
void synapse_pinmux_set_level(const uint8_t gpio_pin, const bool level)
{
	port_pin_set_output_level(gpio_pin, level);
}

int synapse_pinmux_get_level(const uint8_t gpio_pin)
{
	return (port_pin_get_input_level(gpio_pin)!=0);
}

void synapse_pinmux_toggle_level(const uint8_t gpio_pin)
{
	port_pin_toggle_output_level(gpio_pin);
}
